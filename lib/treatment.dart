import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class TreatmentsScreen extends StatefulWidget {
  @override
  _TreatmentsScreenState createState() => _TreatmentsScreenState();
}

class _TreatmentsScreenState extends State<TreatmentsScreen> {
  DateTime? _selectedDate;
  String? _selectedTreatmentType;
  String? _selectedField;
  String? _selectedPlant;
  String? _selectedPesticideProduct;
  String? _selectedQuantity;
  String? _selectedStatus;
  String? _notes;

  final List<String> _treatmentTypes = [
    'Herbicide',
    'Fungicide',
    'Insecticide'
  ];
  final List<String> _fields = ['Field A', 'Field B', 'Field C'];
  final List<String> _plants = ['Plant A', 'Plant B', 'Plant C'];
  final List<String> _pesticideProducts = ['Rocket', 'Weed Master', '2-4D'];
  final List<String> _quantities = ['5 ml', '10 ml', '20 ml', '30 ml'];
  final List<String> _status = ['Pending', 'Completed'];

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = (await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2101),
    ))!;

    // ignore: unnecessary_null_comparison
    if (picked != null && picked != _selectedDate) {
      setState(() {
        _selectedDate = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add New Treatment'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextButton(
              onPressed: () => _selectDate(context),
              child: Text(_selectedDate == null
                  ? 'Select Treatment Date'
                  : 'Treatment Date: ${_selectedDate.toString().split(' ')[0]}'),
            ),
            SizedBox(height: 16),
            DropdownButton<String>(
              value: _selectedTreatmentType,
              hint: Text('Select Treatment Type'),
              onChanged: (newValue) {
                setState(() {
                  _selectedTreatmentType = newValue;
                });
              },
              items: _treatmentTypes
                  .map((type) => DropdownMenuItem(
                        value: type,
                        child: Text(type),
                      ))
                  .toList(),
            ),
            SizedBox(height: 16),
            DropdownButton<String>(
              value: _selectedField,
              hint: Text('Select Field'),
              onChanged: (newValue) {
                setState(() {
                  _selectedField = newValue;
                });
              },
              items: _fields
                  .map((field) => DropdownMenuItem(
                        value: field,
                        child: Text(field),
                      ))
                  .toList(),
            ),
            SizedBox(height: 16),
            DropdownButton<String>(
              value: _selectedPlant,
              hint: Text('Select Plant'),
              onChanged: (newValue) {
                setState(() {
                  _selectedPlant = newValue;
                });
              },
              items: _plants
                  .map((plant) => DropdownMenuItem(
                        value: plant,
                        child: Text(plant),
                      ))
                  .toList(),
            ),
            SizedBox(height: 16),
            DropdownButton<String>(
              value: _selectedPesticideProduct,
              hint: Text('Select Pesticide Product'),
              onChanged: (newValue) {
                setState(() {
                  _selectedPesticideProduct = newValue;
                });
              },
              items: _pesticideProducts
                  .map((product) => DropdownMenuItem(
                        value: product,
                        child: Text(product),
                      ))
                  .toList(),
            ),
            SizedBox(height: 16),
            DropdownButton<String>(
              value: _selectedQuantity,
              hint: Text('Select Quantity'),
              onChanged: (newValue) {
                setState(() {
                  _selectedQuantity = newValue;
                });
              },
              items: _quantities
                  .map((quantity) => DropdownMenuItem(
                        value: quantity,
                        child: Text(quantity),
                      ))
                  .toList(),
            ),
            SizedBox(height: 16),
            DropdownButton<String>(
              value: _selectedStatus,
              hint: Text('Select Status'),
              onChanged: (newValue) {
                setState(() {
                  _selectedStatus = newValue;
                });
              },
              items: _status
                  .map((status) => DropdownMenuItem(
                        value: status,
                        child: Text(status),
                      ))
                  .toList(),
            ),
            SizedBox(height: 16),
            TextField(
              decoration: InputDecoration(labelText: 'Notes'),
              onChanged: (value) {
                setState(() {
                  _notes = value;
                });
              },
            ),
            SizedBox(height: 16),
            ElevatedButton(
              onPressed: () async {
                // Add treatment data to Firestore or perform desired action
                // You can access the selected values using _selectedDate, _selectedTreatmentType, etc.
                // You can also use _notes for short notes
                try {
                  await FirebaseFirestore.instance
                      .collection('treatments')
                      .add({
                    'date': _selectedDate,
                    'status': _selectedStatus,
                    'type': _selectedTreatmentType,
                    'field': _selectedField,
                    'plant': _selectedPlant,
                    'pesticide': _selectedPesticideProduct,
                    'quantity': _selectedQuantity,
                    'notes': _notes,
                  });
                  // Show a success message or navigate back to the previous screen
                } catch (e) {
                  // Handle errors, show an error message, or take appropriate action
                }
              },
              child: Text('Add Treatment'),
            ),
          ],
        ),
      ),
    );
  }
}
